from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'ALYA ISTI SAFIRA' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 6, 23) #TODO Implement this, format (Year, Month, Date)
npm = 1706984511 # TODO Implement this
jurusan = 'Information Systems'
nama_kampus = 'Universitas Indonesia'
hobi = 'Songwriting, Planning, Daydreaming, Reading, and Jamming to music'
deskripsi1 = 'I describe myself in four words. Active​, ​Positive​, ​Expressive, ​Sensitive​.'
deskripsi2 = 'I love to concept things, and marketing is my passion.'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'major': jurusan, 'campus': nama_kampus,
                    'hobbies': hobi, 'desc1': deskripsi1, 'desc2': deskripsi2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
